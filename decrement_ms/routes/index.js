var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/decrement_operation', function(req, res, next){
	res.json({response: 'values'});
});

router.post('/decrement_operation', function(req, res, next){
	console.log("req.params.first" + req.params.first);
	console.log("req.params.second" + req.params.second);
	res.json({response: 'values'});
});

module.exports = router;
