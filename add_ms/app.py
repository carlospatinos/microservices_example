from flask import Flask, request, jsonify
from decimal import Decimal

import json
import time
import redis


app = Flask(__name__)
#redis
#cache = redis.Redis(host='redis', port=6379)
cache = redis.StrictRedis(host='redis', port=6379, decode_responses=True)

def increment_visit_counter():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False


def record_operation(result):
    retries = 5
    while True:
        try:
            counter = cache.get(result)
            if(RepresentsInt(counter)):
                return cache.set(result, int(counter)+1)
            else:
                return cache.set(result, 1)
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/')
def increment_visit_counter():
    count = get_hit_count()
    return 'Welcome, you have visited {} times our service.\n'.format(count)

@app.route('/add_operation', methods=["POST"])
def add_operation():
    first = request.json['first']
    second = request.json['second']
    result = float(first) + float(second);
    data = {}
    data['first'] = first
    data['second'] = second
    data['result'] = result
    record_operation(data)
    return str(result)

@app.route('/add_operation', methods=["GET"])
def get_operation():
    keys = cache.keys()
    vals = cache.mget(keys)
    kv = zip(keys, vals)
    return jsonify("Done1" + str(keys) + str(vals))

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)