dotFiles=`find ./ -name "*.dot"`
for f in $dotFiles
do
	newName=`basename $f .dot`
	cat $f | docker run --rm -i vladgolubev/dot2png > $newName.png
done